package com.devcamp.shop24h.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.entity.Customer;
import com.devcamp.shop24h.entity.Order;
import com.devcamp.shop24h.entity.Payment;
import com.devcamp.shop24h.repository.CustomerRepo;
import com.devcamp.shop24h.repository.PaymentRepo;



@CrossOrigin
@RestController
public class PaymentController {
	@Autowired
	PaymentRepo paymentRepo;
	
	@Autowired
	CustomerRepo customerRepo;
	
	@GetMapping("/payments")
	public ResponseEntity<Object> getAllPayments() {
		try {
			return new ResponseEntity<>(paymentRepo.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/payments/month")
	public ResponseEntity<Object> getPaymentMonth() {
		try {
			return new ResponseEntity<>(paymentRepo.getTotalPaymentByMonth(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/payments/week")
	public ResponseEntity<Object> getPaymentWeek() {
		try {
			return new ResponseEntity<>(paymentRepo.getTotalPaymentByWeek(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/payments/day")
	public ResponseEntity<Object> getPaymenDay() {
		try {
			return new ResponseEntity<>(paymentRepo.getTotalPaymentByDay(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/payments/{paymentId}")
	public ResponseEntity<Object> getPaymentById(@PathVariable Integer paymentId) {
		try {
			Optional<Payment> paymentFound = paymentRepo.findById(paymentId);
			if (paymentFound.isPresent()) {
				return new ResponseEntity<>(paymentFound.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/payments")
	public ResponseEntity<Object> createPayment(@Valid @RequestBody Payment newPayment) {
		try {
			return new ResponseEntity<>(paymentRepo.save(newPayment), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/payments/{customerId}")
	public ResponseEntity<Object> createPayment (@PathVariable("customerId") Long customerId ,@Valid @RequestBody Payment cPayment) {
		try {
			Optional<Customer> customerFound = customerRepo.findById(customerId); 
			if (customerFound.isPresent()) {
				Payment newPayment = new Payment();
				newPayment.setCheckNumber(cPayment.getCheckNumber());
				newPayment.setPaymentDate(cPayment.getPaymentDate());
				newPayment.setAmount(cPayment.getAmount());
				

				Customer _customer = customerFound.get();
				newPayment.setCustomerId(_customer);
			
			return new ResponseEntity<>(paymentRepo.save(newPayment), HttpStatus.CREATED);
		}
			} 
			catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	@PutMapping("/payments/{paymentId}")
	public ResponseEntity<Object> updatePayment(@PathVariable Integer paymentId, @Valid @RequestBody Payment newPayment) {
		try {
			Optional<Payment> paymentFound = paymentRepo.findById(paymentId);
			if (paymentFound.isPresent()) {
				Payment updatePayment = paymentFound.get();
				updatePayment.setAmount(newPayment.getAmount());
				updatePayment.setCheckNumber(newPayment.getCheckNumber());
				updatePayment.setPaymentDate(newPayment.getPaymentDate());
				return new ResponseEntity<>(paymentRepo.save(updatePayment), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/payments/{paymentId}")
	public ResponseEntity<Object> deletePaymentById(@PathVariable Integer paymentId) {
		try {
			Optional<Payment> paymentFound = paymentRepo.findById(paymentId);
			if (paymentFound.isPresent()) {
				paymentRepo.deleteById(paymentId);
				return new ResponseEntity<>( HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/payments")
	public ResponseEntity<Object> deleteAllPayment() {
		try {
			paymentRepo.deleteAll();
			return new ResponseEntity<>( HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
