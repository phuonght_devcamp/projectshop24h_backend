package com.devcamp.shop24h.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.entity.Customer;
import com.devcamp.shop24h.entity.Order;
import com.devcamp.shop24h.repository.CustomerRepo;
import com.devcamp.shop24h.repository.OrderRepo;



@RestController
@CrossOrigin
public class OrderController {
	@Autowired
	OrderRepo orderRepo;
	
	@Autowired
	CustomerRepo customerRepo;
	
	@GetMapping("/orders")
	public ResponseEntity<Object> getAllOrder() {
		try {
			return new ResponseEntity<>(orderRepo.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/orders/customers/{id}")
	public ResponseEntity<Object> getOrdersByCustomerId(@PathVariable String id) {
		try {
		return new ResponseEntity<>(orderRepo.findOrderCustomerId(id) 
				, HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
	
	@GetMapping("/orders/month")
	public ResponseEntity<Object> getOrderMonth() {
		try {
			return new ResponseEntity<>(orderRepo.getOrderByMonth(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/orders/week")
	public ResponseEntity<Object> getOrderWeek() {
		try {
			return new ResponseEntity<>(orderRepo.getOrderByWeek(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/orders/{orderId}")
	public ResponseEntity<Object> getOrderById(@PathVariable Integer orderId) {
		try {
			Optional<Order> orderFound = orderRepo.findById(orderId);
			if (orderFound.isPresent()) {
				return new ResponseEntity<>(orderFound.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/orders/{customerId}")
	public ResponseEntity<Object> createOrder (@PathVariable("customerId") Long customerId ,@Valid @RequestBody Order cOrder) {
		try {
			Optional<Customer> customerFound = customerRepo.findById(customerId); 
			if (customerFound.isPresent()) {
				Order newOrder = new Order();
				newOrder.setComments(cOrder.getComments());
				newOrder.setOrderDate(cOrder.getOrderDate());
				newOrder.setRequiredDate(cOrder.getRequiredDate());
				newOrder.setShippedDate(cOrder.getShippedDate());
				newOrder.setStatus(cOrder.getStatus());

				Customer _customer = customerFound.get();
				newOrder.setCustomerId(_customer);
			
			return new ResponseEntity<>(orderRepo.save(newOrder), HttpStatus.CREATED);
		}
			} 
			catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PutMapping("/orders/{orderId}")
	public ResponseEntity<Object> updateOrder(@PathVariable Integer orderId, @Valid @RequestBody Order newOrder) {
		try {
			Optional<Order> orderFound = orderRepo.findById(orderId);
			if (orderFound.isPresent()) {
				Order updateOrder = orderFound.get();
				updateOrder.setComments(newOrder.getComments());
				updateOrder.setOrderDate(newOrder.getOrderDate());
				updateOrder.setRequiredDate(newOrder.getRequiredDate());
				updateOrder.setShippedDate(newOrder.getShippedDate());
				updateOrder.setStatus(newOrder.getStatus());
				return new ResponseEntity<>(orderRepo.save(updateOrder), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/orders/{orderId}")
	public ResponseEntity<Object> deleteOrderById(@PathVariable Integer orderId) {
		try {
			Optional<Order> orderFound = orderRepo.findById(orderId);
			if (orderFound.isPresent()) {
				orderRepo.deleteById(orderId);
				return new ResponseEntity<>( HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/orders")
	public ResponseEntity<Object> deleteAllOrder() {
		try {
			orderRepo.deleteAll();
			return new ResponseEntity<>( HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
