package com.devcamp.shop24h.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.entity.Customer;
import com.devcamp.shop24h.repository.CustomerRepo;
import com.devcamp.shop24h.service.ExcelExporterCustomer;





@CrossOrigin
@RestController
public class CustomerController {
	@Autowired
	CustomerRepo customerRepo;
	
	@GetMapping("/customers")
	public ResponseEntity<Object> getAllCustomer() {
		try {
			return new ResponseEntity<>(customerRepo.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/platinum")
	public ResponseEntity<Object> getAllCustomerPlatinum() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerPlatinum(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/over5")
	public ResponseEntity<Object> getAllCustomerOver5() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerHaveOver5Order(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/over20")
	public ResponseEntity<Object> getAllCustomerOver20() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerHaveOver20Order(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/over10")
	public ResponseEntity<Object> getAllCustomerOver10() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerHaveOver10Order(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/over40")
	public ResponseEntity<Object> getAllCustomerOver40() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerHaveOver40Order(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/platinum1")
	public ResponseEntity<Object> getAllCustomerPlatinum1() {
		try {
			return new ResponseEntity<>(customerRepo.getCustomerPlatinum(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/gold")
	public ResponseEntity<Object> getAllCustomerGold() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerGold(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/silver")
	public ResponseEntity<Object> getAllCustomerSilver() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerSilver(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/vip")
	public ResponseEntity<Object> getAllCustomerVip() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerVip(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/normal")
	public ResponseEntity<Object> getAllCustomerNormal() {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerNormal(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/count-vip-member")
	public ResponseEntity<Object> getCountVipMember() {
		try {
			return new ResponseEntity<>(customerRepo.getCountVipMember(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/customers/{customerId}")
	public ResponseEntity<Object> getCustomerById(@PathVariable Long customerId) {
		try {
			Optional<Customer> customerFound = customerRepo.findById(customerId);
			if (customerFound.isPresent()) {
				return new ResponseEntity<>(customerFound.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@GetMapping("/customers/phone/{phone}")
	public ResponseEntity<Object> getCustomerByPhone(@PathVariable String phone) {
		try {
		return new ResponseEntity<>(customerRepo.findCustomerByPhone(phone) 
				, HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
	
	@GetMapping("/customers/firstname/{firstName}/lastname/{lastName}")
	public ResponseEntity<Object> getCustomerByFirstNameOrLastName(@PathVariable String firstName,
			@PathVariable String lastName) {
		try {
			return new ResponseEntity<>(customerRepo.findCustomerByFirstNameAndLastName(firstName, lastName)
					, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
//	@GetMapping("/customers/city/{city}/state/{state}")
//	public ResponseEntity<Object> getCustomerByCityAndState(@PathVariable String city,
//			@PathVariable String state) {
//		try {
//			return new ResponseEntity<>(customerRepo.findCustomerByCityAndSate(city, state, PageRequest.of(1, 3)) 
//					, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//	
	@PostMapping("/customers")
	public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer newCustomer) {
		try {
			return new ResponseEntity<>(customerRepo.save(newCustomer), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/customers/{customerId}")
	public ResponseEntity<Object> updateCustomer(@Valid @RequestBody Customer newCustomer, @PathVariable Long customerId) {
		try {
			Optional<Customer> customerFound = customerRepo.findById(customerId);
			if (customerFound.isPresent()) {
				Customer updateCustomer = customerFound.get();
				updateCustomer.setAddress(newCustomer.getAddress());
				updateCustomer.setCity(newCustomer.getCity());
				updateCustomer.setCountry(newCustomer.getCountry());
				updateCustomer.setCreditLimit(newCustomer.getCreditLimit());
				updateCustomer.setFirstName(newCustomer.getFirstName());
				updateCustomer.setLastName(newCustomer.getLastName());
				updateCustomer.setPhoneNumber(newCustomer.getPhoneNumber());
				updateCustomer.setPostalCode(newCustomer.getPostalCode());
				updateCustomer.setSalesRepEmployeeNumber(newCustomer.getSalesRepEmployeeNumber());
				updateCustomer.setState(newCustomer.getState());
				return new ResponseEntity<>(customerRepo.save(updateCustomer), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/customers")
	public ResponseEntity<Object> deleteAllCustomer() {
		try {
			customerRepo.deleteAll();
			return new ResponseEntity<>( HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/customers/{customerId}")
	public ResponseEntity<Object> deleteCustomerById(@PathVariable Long customerId) {
		try {
			Optional<Customer> customerFound = customerRepo.findById(customerId);
			if (customerFound.isPresent()) {
				customerRepo.deleteById(customerId);
				return new ResponseEntity<>( HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage() ,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/export/customers/platinum/excel")
	public void exportPlatinumToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_platinum_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerPlatinum().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	@GetMapping("/export/customers/gold/excel")
	public void exportGoldToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_gold_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerGold().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	
	@GetMapping("/export/customers/silver/excel")
	public void exportSilverToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_silver_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerSilver().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	
	@GetMapping("/export/customers/vip/excel")
	public void exportVipToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_vip_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerVip().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	
	@GetMapping("/export/customers/normal/excel")
	public void exportNormalToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_normal_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerVip().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	
	@GetMapping("/export/customers/over5/excel")
	public void exportBuyerOver5ToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_over5_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerHaveOver5Order().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	
	@GetMapping("/export/customers/over10/excel")
	public void exportBuyerOver10ToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_over10_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerHaveOver10Order().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	
	@GetMapping("/export/customers/over20/excel")
	public void exportBuyerOver20ToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_over20_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerHaveOver20Order().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
	
	@GetMapping("/export/customers/over40/excel")
	public void exportBuyerOver40ToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=_over40_customer" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRepo.getCustomerHaveOver40Order().forEach(customer::add);
		ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(customer);
		excelExporter.export(response);
	}
}
