package com.devcamp.shop24h.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "t_user")
@Getter
@Setter
public class User extends BaseEntity {
	@Column(name = "email")
    private String email;
	
	@NotEmpty(message = "username can't be empty")
	@Column(name = "username", nullable = false, unique = true)
    private String username;

	@NotEmpty(message = "password can't be empty")
	@Column(name = "password", nullable = false, unique = true)
    private String password;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinTable(name = "t_user_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<Role> roles = new HashSet<>();

    
   
	

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User(String email, @NotEmpty(message = "username can't be empty") String username,
			@NotEmpty(message = "password can't be empty") String password, Set<Role> roles) {
		super();
		this.email = email;
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public User(@NotEmpty(message = "username can't be empty") String username,
			@NotEmpty(message = "password can't be empty") String password, Set<Role> roles) {
		super();
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

	public User() {
		super();
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the roles
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	

	

}
