package com.devcamp.shop24h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.devcamp.shop24h.property.FileStorageProperties;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties({
    FileStorageProperties.class
})

public class Shop24hProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(Shop24hProjectApplication.class, args);
	}

}
