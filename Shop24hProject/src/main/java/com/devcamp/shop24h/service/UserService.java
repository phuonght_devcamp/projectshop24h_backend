package com.devcamp.shop24h.service;

import org.springframework.stereotype.Service;

import com.devcamp.shop24h.entity.User;
import com.devcamp.shop24h.security.UserPrincipal;




public interface UserService {
    User createUser(User user);

    UserPrincipal findByUsername(String username);
}
