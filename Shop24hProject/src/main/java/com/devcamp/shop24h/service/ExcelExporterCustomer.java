package com.devcamp.shop24h.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.shop24h.entity.Customer;





/**
 * @author HieuHN
 *
 */
public class ExcelExporterCustomer {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	
	private List<Customer> customers;
	

	/**
	 * Constructor khởi tạo server export danh sách customer 
	 * @param orders
	 */
	public ExcelExporterCustomer(List<Customer> customers) {
		this.customers = customers;
		workbook = new XSSFWorkbook();
	}
	
	/**
	 * Tạo các ô cho excel file.
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 */
	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	/**
	 * Khai báo cho sheet và các dòng đầu tiên
	 */
	private void writeHeaderLine() {
		sheet = workbook.createSheet("Orders");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "Id", style);
		createCell(row, 1, "Firstname", style);
		createCell(row, 2, "Lastname", style);
		createCell(row, 3, "Address ", style);
		createCell(row, 4, "City", style);
		createCell(row, 5, "State", style);
		createCell(row, 6, "Country", style);
		createCell(row, 7, "Phone Number", style);
		
		createCell(row, 8, "Postal Code", style);
		createCell(row, 9, "Credit Limit", style);
		createCell(row, 10, "Sale Rep", style);
		

	}

	
	/**
	 * fill dữ liệu cho các dòng tiếp theo.
	 */
	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (Customer customer : this.customers) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			
			
             
             
			Long id = customer.getId();
			String firstName = customer.getFirstName();
			String lastName = customer.getLastName();
			String address = customer.getAddress();
			String city = customer.getCity();
			String state = customer.getState();
			String country = customer.getCountry();
			String phoneNumber = customer.getPhoneNumber();
			String postalCode = customer.getPostalCode();
			int creditLimit = customer.getCreditLimit();
			int saleRep = customer.getSalesRepEmployeeNumber();
			
		
			
			

			createCell(row, columnCount++, String.valueOf(id), style);
			createCell(row, columnCount++, firstName, style);
			createCell(row, columnCount++, lastName, style);
			createCell(row, columnCount++, address, style);
			createCell(row, columnCount++, city, style);
			createCell(row, columnCount++, state, style);
			createCell(row, columnCount++, country, style);
			createCell(row, columnCount++, phoneNumber, style);
			createCell(row, columnCount++, postalCode, style);
			createCell(row, columnCount++, String.valueOf(creditLimit), style);
			createCell(row, columnCount++, String.valueOf(saleRep), style);
			
			
		

		}
	}

	/**
	 * xuất dữ liệu ra dạng file
	 * @param response
	 * @throws IOException
	 */
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
