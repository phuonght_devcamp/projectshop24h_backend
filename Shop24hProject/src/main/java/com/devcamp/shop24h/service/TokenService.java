package com.devcamp.shop24h.service;


import org.springframework.stereotype.Service;

import com.devcamp.shop24h.entity.Token;




public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);
}
