package com.devcamp.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.entity.Order;
import com.devcamp.shop24h.entity.Payment;



public interface PaymentRepo extends JpaRepository<Payment, Integer> {

	Optional<Payment> findById(long paymentId);

	@Query(value = "select MONTHNAME(payment_date) as month , SUM(ammount) as sum  "
			+ "from payments p WHERE payment_date < Now() and payment_date > DATE_ADD(Now(), INTERVAL- 12 MONTH) "
			+ "GROUP BY month  DESC"
			, nativeQuery = true)
	List<Object> getTotalPaymentByMonth();
	
	@Query(value = "select WEEK(payment_date) as week , SUM(ammount) as sum  "
			+ "from payments p WHERE payment_date < Now() and payment_date > DATE_ADD(Now(), INTERVAL- 12 MONTH) "
			+ "GROUP BY week  ASC"
			, nativeQuery = true)
	List<Object> getTotalPaymentByWeek();
	
	@Query(value = "select DATE(payment_date) as date , SUM(ammount) as sum  "
			+ "from payments p WHERE payment_date < Now() and payment_date > DATE_ADD(Now(), INTERVAL- 1 MONTH) "
			+ "GROUP BY date  ASC"
			, nativeQuery = true)
	List<Object> getTotalPaymentByDay();
}
