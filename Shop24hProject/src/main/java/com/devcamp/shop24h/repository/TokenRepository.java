package com.devcamp.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.shop24h.entity.Token;



public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);
}
