package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.shop24h.entity.Customer;



@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {
	@Query(value = "select * "
			+ "from customers c "
			+ "where c.first_name like %:firstName% or "
			+ "c.last_name like %:lastName%", nativeQuery = true)
	List<Customer> findCustomerByFirstNameAndLastName(@Param("firstName") String firstName,
			@Param("lastName") String lastName);
	
	@Query(value = "select * "
			+ "from customers c "
			+ "where c.phone_number like %:phone% "
			, nativeQuery = true)
	List<Customer> findCustomerByPhone(@Param("phone") String phone);
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE b.total >50000 order by total DESC"
			
			, nativeQuery = true)
	List<Object> findCustomerPlatinum();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE 50000>= b.total  && b.total >20000 order by total DESC"
			
			, nativeQuery = true)
	List<Object> findCustomerGold();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE 20000>= b.total && b.total  >10000  order by total DESC"
			
			, nativeQuery = true)
	List<Object> findCustomerSilver();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE b.total >5000  && b.total <=10000 order by total DESC"
			
			, nativeQuery = true)
	List<Object> findCustomerVip();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE b.total >50000 order by total DESC"
			
			, nativeQuery = true)
	List<Customer> getCustomerPlatinum();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE b.total >20000 && b.total <= 50000 order by total DESC"
			
			, nativeQuery = true)
	List<Customer> getCustomerGold();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE b.total >10000 && b.total <=20000 order by total DESC"
			
			, nativeQuery = true)
	List<Customer> getCustomerSilver();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE b.total >5000 && b.total <=10000 order by total DESC"
			
			, nativeQuery = true)
	List<Customer> getCustomerVip();
	
	@Query(value = "SELECT * from customers c INNER JOIN ( SELECT customer_id , "
			+ "SUM(ammount ) as total "
			+ "FROM payments p GROUP by customer_id ) b on c.id = b.customer_id "
			+ "WHERE b.total <=5000  order by total DESC"
			
			, nativeQuery = true)
	List<Object> findCustomerNormal();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >5 order by total DESC"
			, nativeQuery = true)
	List<Object> findCustomerHaveOver5Order();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >10 order by total DESC"
			, nativeQuery = true)
	List<Object> findCustomerHaveOver10Order();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >20 order by total DESC"
			, nativeQuery = true)
	List<Object> findCustomerHaveOver20Order();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >40 order by total DESC"
			, nativeQuery = true)
	List<Object> findCustomerHaveOver40Order();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >5 order by total DESC"
			, nativeQuery = true)
	List<Customer> getCustomerHaveOver5Order();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >10 order by total DESC"
			, nativeQuery = true)
	List<Customer> getCustomerHaveOver10Order();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >20 order by total DESC"
			, nativeQuery = true)
	List<Customer> getCustomerHaveOver20Order();
	
	@Query(value = "SELECT * from customers c "
			+ "INNER JOIN ( SELECT customer_id , COUNT(id ) as total FROM orders o GROUP by customer_id ) b "
			+ "on c.id = b.customer_id "
			+ "WHERE b.total >40 order by total DESC"
			, nativeQuery = true)
	List<Customer> getCustomerHaveOver40Order();
	
	@Query(value = "SELECT SUM(CASE WHEN b.total <5000 then 1 else 0 end) as normal,\r\n"
			+ "SUM(CASE WHEN b.total >5000 && b.total <=10000 then 1 else 0 end) as vip,\r\n"
			+ "SUM(CASE WHEN b.total >10000 && b.total <=20000 then 1 else 0 end) as silver,\r\n"
			+ "SUM(CASE WHEN b.total >20000 && b.total <=50000 then 1 else 0 end) as gold,\r\n"
			+ "SUM(CASE WHEN b.total >50000  then 1 else 0 end) as platinum from(SELECT customer_id , \r\n"
			+ "			SUM(ammount ) as total  \r\n"
			+ "			FROM payments p GROUP by customer_id   ) b"
			, nativeQuery = true)
	List<Object> getCountVipMember();
	
			
}
