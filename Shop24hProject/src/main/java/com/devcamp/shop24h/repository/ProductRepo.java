package com.devcamp.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.entity.Order;
import com.devcamp.shop24h.entity.Product;



public interface ProductRepo extends JpaRepository<Product, Integer> {
	
	@Query(value = "SELECT * from products p INNER JOIN ( SELECT product_id , COUNT(quantity_order ) as count FROM `order_details` o GROUP by product_id LIMIT 0,10 ) b on p.id = b.product_id "
			, nativeQuery = true)
	List<Object> findTopProduct();
	
	@Query(value = "SELECT * FROM products order BY created_at DESC  LIMIT 0,5 "
			, nativeQuery = true)
	List<Object> findLatestproduct();
	
	@Query(value = "select * "
			+ "from products p "
			+ "where p.product_name like %:productName% "
			, nativeQuery = true)
	List<Product> findProductByProductName(@Param("productName") String productName);
	
	
	@Query(value = "select * "
			+ "from products p "
			+ "where p.product_line_id like %:productLineId% "
			, nativeQuery = true)
	List<Product> findProductByProductLineId(@Param("productLineId") String productLineId);

	Optional<Product> findById(Long productId);
	
	@Query(value = "SELECT * FROM `products` p WHERE p.buy_price<=700"
			, nativeQuery = true)
	List<Object> findUnder700Product();
	
	@Query(value = "SELECT * FROM `products` p WHERE p.buy_price>700 && p.buy_price<=1500"
			, nativeQuery = true)
	List<Object> find700to1500Product();
	
	@Query(value = "SELECT * FROM `products` p WHERE p.buy_price>1500 && p.buy_price<=3000"
			, nativeQuery = true)
	List<Object> find1500to3000Product();
	
	@Query(value = "SELECT * FROM `products` p"
			, nativeQuery = true)
	List<Object> findAllProduct();
}
