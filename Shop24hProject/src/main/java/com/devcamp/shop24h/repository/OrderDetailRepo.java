package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.shop24h.entity.Order;
import com.devcamp.shop24h.entity.OrderDetail;


@Repository
public interface OrderDetailRepo extends JpaRepository<OrderDetail, Integer> {

	@Query(value = "select * "
			+ "from order_details o "
			+ "where o.order_id like %:orderId% "
			, nativeQuery = true)
	List<OrderDetail> findOrderDetailByOrderId(@Param("orderId") String orderId);
		
		
}
