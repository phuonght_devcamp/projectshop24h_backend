package com.devcamp.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.entity.Order;



public interface OrderRepo extends JpaRepository<Order, Integer> {

	@Query(value = "select * "
			+ "from orders o "
			+ "where o.customer_id like %:customerId% "
			, nativeQuery = true)
	List<Order> findOrderCustomerId(@Param("customerId") String customerId);
	
	@Query(value = "select * \r\n"
			+ "			from orders o  WHERE order_date < Now() and order_date > DATE_ADD(Now(), INTERVAL- 1 MONTH) \r\n"
			+ "			  ORDER BY order_date DESC"
			, nativeQuery = true)
	List<Order> getOrderByMonth();
	
	@Query(value = "select * \r\n"
			+ "			from orders o  WHERE order_date < Now() and order_date > DATE_ADD(Now(), INTERVAL- 7 DAY"
			+ ") \r\n"
			+ "			  ORDER BY order_date DESC"
			, nativeQuery = true)
	List<Order> getOrderByWeek();

	Optional<Order> findById(Long orderId);
}
