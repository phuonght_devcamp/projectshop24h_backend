# Shop24h: uStora

# 1 - Mô tả chung


1.1 Tên chương trình
Shop24h: Shop bán hàng điện thoại và phụ kiện

1.2 Mục đích ứng dụng
Mục đích chính của việc tạo ra ứng dụng này là thực hành những kiến ​​thức mà tôi thu thập được cho đến nay về việc tạo các ứng dụng từ cơ sở dữ liệu bằng cách sử dụng Spring.

Mục đích của ứng dụng Shop24h là để hỗ trợ một cửa hàng bán cellphone. Nhiệm vụ chính của ứng dụng là cho phép khách hàng duyệt qua danh sách hàng hoá và thông tin chi tiết của họ, sau đó đặt một item đã chọn. Ứng dụng còn có bảng khách hàng cho phép xem đơn hàng và thay đổi cài đặt tài khoản, bảng quản trị viên cho phép quản lý đơn hàng và người dùng, thêm sửa product, chỉnh s-ửa product. Ứng dụng này cũng có hệ thống đăng nhập và đăng ký cho khách hàng mới.

1.3 Bản tóm tắt giới thiệu ngắn gọn về ứng dụng

Backend:
Rest API được viết bằng Spring sử dụng các công nghệ sau:

- Spring Security
- Spring Data JPA
- Hibernate

FrontEnd:
Giao diện người dùng chủ yếu được viết bằng các công nghệ sau: 

- Bootstrap
- HTML, CSS
- JavaScript

1.4 Đối tượng sử dụng


- Admin - người quản lý website và có nhiều đặc quyền nhất
- Khách hàng - khách hàng, người sử dụng trang của chúng tôi để đặt hàng, mỗi người dùng tạo tài khoản ban đầu có vai trò khách hàng, các vai trò khác do quản trị viên phân công.
- Nhân viên: thực hiện quản lý đơn hàng, khách hàng


# 2 - Mô tả các chức năng chính

2.1 Vận hành
Để chạy, bạn sẽ cần:
Máy chủ MySQL PhpMyAdmin,Apache Tomcat


2.2 Chức năng

- Liệt kê danh mục sản phẩm, dữ liệu sản phẩm lấy từ back-end.
- Chọn sản phẩm cần mua vào giỏ hàng, 
- Chọn nhiều lần thì tăng số sản phẩm trong giỏ hàng lên.
- Mục giỏ hàng cho phép tăng giảm số lượng sản phẩm.
- Khi nhấn nút mua hàng  trong mục giỏ hàng sẽ tạo 1 Order trong có các sản phẩm đã chọn, số tiền mỗi loại theo số lượng chọn và tổng tiền.
- Nhấn nút thanh toán tại Order thì sẽ ra mục nhập thông tin người mua.
- Sau khi nhập xong dữ liệu sẽ được lưu vào DB qua back-end. 
- Nếu số điện thoại(sdt) của khách trùng với sdt  đã có trong DB thì sẽ cập nhật thông tin khách hàng đã có với sdt đó và add thông tin order mới này cho khách hàng đó. 
- Nếu sdt không trùng thì tạo mới khách hàng và oder mới trong DB
- Chức năng filter/phân trang hoạt động tốt, filter với 3 loại thuộc tính: giá, product line, hot product.
- Giao diện front-end chạy được responsive.
- Cho phép thêm sửa xóa upload ảnh sản phẩm,  dữ liệu sản phẩm lấy từ back-end.

